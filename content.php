<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>
	</title>
	<link rel="stylesheet" type="text/css" href="css/content.css">
	<link rel="stylesheet" type="text/css" href="css/chat.css">
	<link rel="stylesheet" type="text/css" href="css/content_buttons.css">
	<link rel="stylesheet" type="text/css" href="css/character.css">
	<link rel="stylesheet" type="text/css" href="css/expedition.css">
	<link rel="stylesheet" type="text/css" href="css/messages.css">
	<link href='https://fonts.googleapis.com/css?family=Audiowide&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Titillium+Web&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	

	<script src="js/jquery.js"></script>
	<script type="text/javascript" src="chat/chat.js"></script>
	<script type="text/javascript" src="chat/chat-keydown.js"></script>
	<script src="js/mouse-box.js"></script>
	
	<script>

	</script>
	
</head>
<body onload="process(); ">
<?php
session_start();
if(!isset($_SESSION['zalogowany']) && $_SESSION['zalogowany']!=true) //sprawdza czy uzytkownik jest zalogowany
{	
	header('Location: php/login_page.php');
	exit(); 
}

require_once "resources_status.php";
?>

<div class="contener">

	<div class="resources"> 
	<h3 class="login"><?php echo $_SESSION['login_S']?></h3>
		<table>
			<tr>
			<th> <img class="resources" src="css/img/gold.png" ></th><th><?php echo $_SESSION['gold']?></th>
			<th> <img class="resources" src="css/img/diamond.png"></th><th><?php echo $_SESSION['diamond']?></th>
			<th> <img class="resources" src="css/img/xp.png"></th><th><?php echo $_SESSION['experience']?>/<?php echo $_SESSION['experience_to_level']?></th>
			<th> <img class="resources" src="css/img/lvl.png"></th><th><?php echo $_SESSION['level']?></th>
			</tr>
		</table>
	</div>
	
	<a href="content.php?page=character"><div class="logo"></div></a>
	<a href="php/logout.php" id="logout"><input type="submit" value="Wyloguj"/></a>
	<br />
	<div class="menu">
		<div class="menu-content">
			<a href="content.php?page=character" class="menu_button_character">Postać</a><br>
			<a href="content.php?page=messages" class="menu_button_messages">Wiadomości</a>
			<a href="content.php?page=expedition" class="menu_button_expedition">Wyprawa</a><br>
			<a href="content.php?page=work" class="menu_button_work">Praca</a>
		</div>
	</div>
	<div class="content">
	<div class="contentvalue">
<?php
	if(isset($_GET['page']))
	{
		if($_GET['page']=='work')
		{
			require_once "engine/work.php";
		}
		
		
		if($_GET['page']=='messages')				//wiadomości
		{
			require_once "engine/messages.php";
		
		}
			if($_GET['page']=='new_messages')
			{
				require_once "engine/messages/new_messages.php";
			}
			if($_GET['page']=='look_messages')
			{
				require_once "engine/messages/look_messages.php";
			}
			if($_GET['page']=='sender_messages')
			{
				require_once "engine/messages/sender_messages.php";
			}
			
			
		if($_GET['page']=='expedition')
		{
			require_once "engine/expedition.php";
		}
		
		
		if($_GET['page']=='character')
		{
			require_once "engine/character.php";
		}
	}


?>
	</div>
	</div>
		<div id="chatBox">
			<div id="divMessage"></div>
			<input type="text" id="chatMessage" />
			<input type="submit" value="Wyślij" onclick="savedata(); process();"/>
			<input type="button" value="Odśwież" onclick="process();"/ >
		</div>
</div>
</body>
</html>