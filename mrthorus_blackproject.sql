-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: 192.168.101.146
-- Czas wygenerowania: 24 Kwi 2018, 12:52
-- Wersja serwera: 5.6.36-82.1-log
-- Wersja PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `mrthorus_blackproject`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` time NOT NULL,
  `login` varchar(40) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin2 AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `chat`
--

INSERT INTO `chat` (`id`, `time`, `login`, `message`) VALUES
(1, '12:46:33', 'test', 'test');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` varchar(20) NOT NULL,
  `receiver` varchar(20) NOT NULL,
  `subject` varchar(20) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `text` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `date` date NOT NULL,
  `sender_del` int(1) NOT NULL DEFAULT '0',
  `receiver_del` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `password_token`
--

CREATE TABLE IF NOT EXISTS `password_token` (
  `token` varchar(255) NOT NULL,
  `email` varchar(20) NOT NULL,
  `expire_date` int(255) NOT NULL,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `resources`
--

CREATE TABLE IF NOT EXISTS `resources` (
  `login` varchar(20) NOT NULL,
  `gold` int(200) NOT NULL,
  `diamond` int(200) NOT NULL,
  `experience` int(200) NOT NULL,
  `experience_to_level` int(200) NOT NULL,
  `level` int(10) NOT NULL,
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `resources`
--

INSERT INTO `resources` (`login`, `gold`, `diamond`, `experience`, `experience_to_level`, `level`) VALUES
('mrthorus', 100, 10, 0, 30, 1),
('test', 100, 10, 0, 30, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `statistics`
--

CREATE TABLE IF NOT EXISTS `statistics` (
  `login` varchar(20) NOT NULL,
  `strenght` int(20) NOT NULL,
  `dexterity` int(20) NOT NULL,
  `intelligence` int(20) NOT NULL,
  `vitality` int(20) NOT NULL,
  `points` int(20) NOT NULL,
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `statistics`
--

INSERT INTO `statistics` (`login`, `strenght`, `dexterity`, `intelligence`, `vitality`, `points`) VALUES
('mrthorus', 10, 5, 5, 6, 24),
('test', 6, 5, 5, 5, 9);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `statistics_values`
--

CREATE TABLE IF NOT EXISTS `statistics_values` (
  `login` varchar(20) NOT NULL,
  `obrazenia_maks` int(20) NOT NULL,
  `obrazenia_min` int(20) NOT NULL,
  `obrazenia_krytyczne` int(20) NOT NULL,
  `szansa_na_cios_krytyczny` int(20) NOT NULL,
  `szansa_na_unik` int(20) NOT NULL,
  `punkty_umiejetnosci` int(20) NOT NULL,
  `punkty_zycia` int(20) NOT NULL,
  `obrona` int(20) NOT NULL,
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `statistics_values`
--

INSERT INTO `statistics_values` (`login`, `obrazenia_maks`, `obrazenia_min`, `obrazenia_krytyczne`, `szansa_na_cios_krytyczny`, `szansa_na_unik`, `punkty_umiejetnosci`, `punkty_zycia`, `obrona`) VALUES
('mrthorus', 20, 15, 19, 4, 3, 5, 80, 12),
('test', 12, 9, 11, 4, 3, 5, 75, 10);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `email`) VALUES
(8, 'mrthorus', '$2y$10$neoDRmUtpqe60ytGRPRdo.IvnpILjtMcloO6i7VKnytUufpALYjFG', 'jabolovp@gmail.com'),
(9, 'test', '$2y$10$xHREtfxyE52wbBaKrINYhO5SXokIdAk9z7YLNhVi0zyfYhARA8Oti', 'test@wp.pl');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `work`
--

CREATE TABLE IF NOT EXISTS `work` (
  `login` varchar(20) NOT NULL,
  `start_time` int(255) NOT NULL,
  `end_time` int(255) NOT NULL,
  `payment` int(100) NOT NULL,
  `experience` int(100) NOT NULL,
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
