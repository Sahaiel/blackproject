﻿<?php
// wygenerujemy dane wyjściowe jako plik XML
session_start();
if(!isset($_SESSION['zalogowany']) && $_SESSION['zalogowany']!=true) //sprawdza czy uzytkownik jest zalogowany
{	
	header('Location: php/login_page.php');
	exit(); 
}

function filtruj($zmienna)
{
    $zmienna=trim($zmienna);
    $zmienna=htmlspecialchars($zmienna);
    $zmienna=addslashes($zmienna);
	return $zmienna;
}

$login =  $_SESSION['login_S'];

require_once "../php/db.php";
	try
	{
		$connect_db = new mysqli($db_host,$db_login,$db_password,$db_name);
		if($connect_db->connect_errno!=0) throw new Exception(mysqli_connect_errno());
		else
		{
			if(!isset($_GET['chatMessage']))
			{
				if(!$rezultat = $connect_db->query("SELECT * FROM chat")) throw new Exception($connect_db->error);
				$ile = $rezultat->num_rows;
				$chat = $rezultat->fetch_all();
				/*echo "<pre>"; 
				print_r($chat);
				echo "</pre>";*/
				
				
				if($ile > 0) 
				{
					$czyszczenie = $chat[$ile-1][0] - 50; //usówa starsze wiadomości
					if(!$connect_db->query("DELETE FROM chat WHERE id < '$czyszczenie'")) throw new Exception($connect_db->error);
				}
				
				
				for($i = $ile -1; $i >= 0; $i--)
				{
					if($chat[$i][2] == $login)
					{
						$nick = "<span class='chatSelfLogin'>".$chat[$i][2];
					}
					else 
					{
						$nick = "<span class='chatLogin'>".$chat[$i][2];
					}
					echo "[<span class='chatTime'>".$chat[$i][1]."</span>] ".$nick."</span>: ".$chat[$i][3]."<br />";
			
				}
				
			}
			else
			{
				$message = $_GET['chatMessage'];
				$message = filtruj($message);
				$time = date("H:i:s");
				if(!$connect_db->query("INSERT INTO chat VALUES (NULL,'$time','$login','$message')")) throw new Exception($connect_db->error);

			}
		}
		$connect_db->close();
	}
	catch(Exception $e) //wyjatek
	{
		echo '<span class="text_error">Błąd serwera.</span>';//
		echo '<br />Informacja developerska: '.$e; //Informacja dla dev
	}
?>


