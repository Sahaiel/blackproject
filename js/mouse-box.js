//do skryptu wymagane jest dodanie diva o id 'mouse-box'
$(document).ready(function(){
	main_mouse_box_script();
});

function main_mouse_box_script() //uruchamiana poprzez funkcję u góry ^
{
	$("body").prepend("<div id='mouse-box' ></div>");
///////Tu dodajemy nowe elementy
	//new_mouse_box("Treść wiadomości", "id elementu strony");
	
						//Zakładka postaci
	new_mouse_box("Zręczność zwiększa szansę na cios krytyczny i na unik.", "#box_dex");
	new_mouse_box("Siła zwiększa zadawane przez ciebie obrażenia, a także wartość obrażeń krytycznych.", "#box_str");
	new_mouse_box("Inteligencja pozwala rozwijać umiejętności.", "#box_int");
	new_mouse_box("Witalność zwiększa obronę i punkty życia.", "#box_vit");
	
	
	
	
////////////////////////////////Tu zmieniamy wygląd mouse-boxa
	$("#mouse-box").css({
	"max-width": "400px",
    "min-height": "30px",
    "background-color": "rgba(0,0,0,0.95)",
    "position": "absolute",
	"padding": "10px",
	"box-shadow": "0px 0px 8px 0px #999",
	"border-radius": "4px",
    "color": "white",
	"display": "none",
	"z-index": "100",
	});
}
function main_mouse_box(tresc_wiadomosci)  {
	
	$('#mouse-box').text(tresc_wiadomosci);
	$(document).mousemove(function(e) {
		$('#mouse-box').css('margin-left', e.pageX + 14);
		$('#mouse-box').css('margin-top', e.pageY + 24);
		
		
	});
	$('#mouse-box').css('display', "block"); 
}

function new_mouse_box(tresc, id)
{
	$(id).hover(function(){
	main_mouse_box(tresc);
	},
	function(){
		$('#mouse-box').css('display', "none");
	});

	
}
