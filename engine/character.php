<!DOCTYPE HTML> 
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title></title>
</head>
<body>

<?php





if(!isset($_SESSION['zalogowany']) && $_SESSION['zalogowany']!=true) //sprawdza czy uzytkownik jest zalogowany
{	
	header('Location: php/login_page.php');
	exit(); 
}

//echo $_SESSION['login_S'];

$login_S = $_SESSION['login_S'];



require_once "php/db.php";
	try
	{
		$connect_db = new mysqli($db_host,$db_login,$db_password,$db_name);
		if($connect_db->connect_errno!=0) throw new Exception(mysqli_connect_errno());
		else
		{
			
			if(!$user = $connect_db->query("SELECT * FROM statistics WHERE login='$login_S'")) throw new Exception($connect_db->error); // pobiera dane z tabeli statystyki które przechowują statystyki bohatera
			
			$stats = $user->fetch_assoc();
			$sila = $stats['strenght'];
			$zrecznosc = $stats['dexterity'];
			$inteligencja = $stats['intelligence'];
			$witalnosc = $stats['vitality'];
			$punkty = $stats['points'];
			//str = siła , dex = zrecznosc , int = inteligencja, vit = witalnosc
					
					//wartości
				
					
					$obrazenia_maks = 2 * $sila;
					$obrazenia_min = round(1.5 * $sila);
					$obrazenia_krytyczne = round($sila * 1.9);
					
					$szansa_na_cios_krytyczny = round($zrecznosc * 0.7);
					$szansa_na_unik = round($zrecznosc * 0.5);
					
					$punkty_umiejetnosci = $inteligencja;
					
					$punkty_zycia = 5 * $witalnosc + 50;
					$obrona = 2 * $witalnosc;
					
					if(!$wartosci = $connect_db->query("SELECT * FROM statistics_values WHERE login='$login_S'")) throw new Exception($connect_db->error);
					
					if($wartosci->num_rows == 0)
					{
						if(!$connect_db->query("INSERT INTO statistics_values VALUES ('$login_S', '$obrazenia_maks', '$obrazenia_min', '$obrazenia_krytyczne', '$szansa_na_cios_krytyczny', '$szansa_na_unik', '$punkty_umiejetnosci', '$punkty_zycia', '$obrona')")) throw new Exception($connect_db->error);
					}
					else
					{
						if(!$pkt = $connect_db->query("SELECT punkty_umiejetnosci FROM statistics_values WHERE login='$login_S'")) throw new Exception($connect_db->error);
						$pkt = $pkt->fetch_assoc();
						$punkty_umiejetnosci = $pkt['punkty_umiejetnosci'];
						
				
						if(!$connect_db->query("UPDATE statistics_values SET obrazenia_maks='$obrazenia_maks', obrazenia_min='$obrazenia_min', obrazenia_krytyczne='$obrazenia_krytyczne', szansa_na_cios_krytyczny='$szansa_na_cios_krytyczny', szansa_na_unik='$szansa_na_unik', punkty_umiejetnosci='$punkty_umiejetnosci', punkty_zycia='$punkty_zycia', obrona='$obrona' WHERE login='$login_S'")) throw new Exception($connect_db->error);
						
						
					}
					
			echo ('
			<table>
				<tr class="text">
					<th height=80px>'.$login_S.'</th>
				</tr>
				<tr class="text">
					<th width=80px height=60px>Punkty do rozdania</th>
					<th width=80px >'.$punkty.'</th>
					
				</tr>
				<tr class="text">
					<th width=80px height=50px id="box_str">Siła</th>
					<th width=80px >'.$sila.'</th>
					<th width=80px ><a class="plus_button" href="content.php?page=character&point=str"></a></th>
					<th width=80px >Obrażenia<br /> '.$obrazenia_min.'-'.$obrazenia_maks.'</th>
					<th width=190px >Obrażenia krytyczne<br />'.$obrazenia_krytyczne.'%</th>
				</tr>
				<tr class="text">
					<th width=80px height=50px id="box_dex">Zręczność</th>
					<th width=80px >'.$zrecznosc.'</th>
					<th width=80px ><a class="plus_button" href="content.php?page=character&point=dex"></a></th>
					<th width=190px >Szansa na cios krytyczny<br />'.$szansa_na_cios_krytyczny.'%</th>
					<th width=190px >Szansa na unik<br />'.$szansa_na_unik.'%</th>
				</tr>
				<tr class="text">
					<th width=80px height=50px id="box_int">Inteligencja</th>
					<th width=80px >'.$inteligencja.'</th>
					<th width=80px ><a class="plus_button" href="content.php?page=character&point=int"></a></th>
					<th width=190px >Wolne punkty umiejętności<br />'.$punkty_umiejetnosci.'</th>
					<th width=190px ><a href="">Umiejętności</a></th>
				</tr>
				<tr class="text">
					<th width=80px height=50px id="box_vit">Witalność</th>
					<th width=80px >'.$witalnosc.'</th>
					<th width=80px ><a class="plus_button" href="content.php?page=character&point=vit"></a></th>
					<th width=80px >Obrona<br />'.$obrona.'</th>
					<th width=80px >Punkty życia<br />'.$punkty_zycia.'</th>
				</tr>
			
			</table>
			');
			
			if($punkty > 0)
			{
				if(isset($_GET['point']))
				{
					if($_GET['point'] == 'str' || $_GET['point'] == 'dex' || $_GET['point'] == 'int' || $_GET['point'] == 'vit')
					{
					$punkty--;
					
					
					
					switch($_GET['point']) //dodaje i aktualizuje statystyki
						{
							case 'str':
								echo "siła";
								$sila++;
								//header('Location: content.php?page=character');
								break;
							case 'dex':
								$zrecznosc++;
								//header('Location: content.php?page=character');
								break;
							case 'int':
								$inteligencja++;
								//header('Location: content.php?page=character');
								break;
							case 'vit':
								$witalnosc++;
								//header('Location: content.php?page=character');
							
								break;
							default:
								header('Location: content.php?page=character');
						}
						
		
						
						if(!$connect_db->query("UPDATE statistics SET strenght='$sila', dexterity='$zrecznosc', intelligence='$inteligencja', vitality='$witalnosc', points='$punkty' WHERE login='$login_S'")) throw new Exception($connect_db->error);
	
						
						
						header('Location: content.php?page=character');
					}
					else
					{
						header('Location: content.php?page=character');
					}
				}
			}
			else
			{
				if(isset($_GET['point']))
				{
					header('Location: content.php?page=character');
				}
			}
		}
		$connect_db->close();
	}
	catch(Exception $e) //wyjatek
	{
		echo '<span class="text_error">Błąd serwera.</span>';//
		echo '<br />Informacja developerska: '.$e; //Informacja dla dev
	}

?>

</body>