

<?php

function filtruj($zmienna)
{
    $zmienna=trim($zmienna);
    $zmienna=htmlspecialchars($zmienna);
    $zmienna=addslashes($zmienna);
	return $zmienna;
}


if(!isset($_SESSION['zalogowany']) && $_SESSION['zalogowany']!=true) //sprawdza czy uzytkownik jest zalogowany
{	
	header('Location: php/login_page.php');
	exit(); 
}

//echo $_SESSION['login_S'];

$login_S = $_SESSION['login_S'];

echo ('


		<form method="POST">
		<table>
			<tr>
				<th>Odbiorca:</th>
				<th><input type="text" style="width: 225px;" name="odbiorca"></th>
			</tr>
			<tr>
				<th>Temat:</th><th><input style="width: 225px" type="text" name="temat"></th> 
			</tr>
			<tr>
				<th colspan="2"><textarea name="text" style="max-height:100px; max-width:300px; min-height:100px; min-width:300px;"></textarea></th>
			</tr>
			<tr>
				<th></th><th><input type="submit" value="Wyślij"></th>
				
			

			</tr>
		</table>
		</form>


');
	if(isset($_SESSION['error_new_messages'])) echo '<span class="text_error">'.$_SESSION['error_new_messages'].'</span><br />'; unset($_SESSION['error_new_messages']);

if(isset($_POST['odbiorca']))
{
	$odbiorca = filtruj($_POST['odbiorca']);
	$temat = filtruj($_POST['temat']);
	$text = filtruj($_POST['text']);
  if($odbiorca != $login_S)
  {
	require_once "php/db.php";
	try
	{
		$connect_db = new mysqli($db_host,$db_login,$db_password,$db_name);
		if($connect_db->connect_errno!=0) throw new Exception(mysqli_connect_errno());
		else
		{
			if(!$istnieje = $connect_db->query("SELECT * FROM users WHERE login='$odbiorca'")) throw new Exception($connect_db->error);
			
			//trzeba zabezpieczyć formnularz przed nieodpowiednimi znakami
			
			if((strlen($odbiorca) < 3) || (strlen($temat) < 1) || (strlen($text) < 1))
			{
			$_SESSION['error_new_messages'] = "Wypełnij wszystkie pola";
			header('Location: content.php?page=new_messages');
			}
			else
			{
				if($istnieje->num_rows>0)
				{
					$data = date("Y-m-d");
					if(!$connect_db->query("INSERT INTO messages VALUES (NULL, '$login_S', '$odbiorca', '$temat', '$text', '$data', 0, 0)")) throw new Exception($connect_db->error);
					$_SESSION['error_new_messages'] = "Wiadomość wysłana";
					header('Location: content.php?page=new_messages'); 
				}
				else
				{
					
					$_SESSION['error_new_messages'] = "Użytkownik nie istnieje";
					header('Location: content.php?page=new_messages');
				}
			}
			
		
			$connect_db->close();
		}
	}
	catch(Exception $e) //wyjatek
	{
		echo '<span class="text_error">Błąd serwera.</span>';//
		echo '<br />Informacja developerska: '.$e; //Informacja dla dev
	}
  }	
  else 
  {
	$_SESSION['error_new_messages'] = "Nie możesz wysłać wiadomości do siebie";
	header('Location: content.php?page=new_messages');
  }
}
?>

