<?php 
session_start(); 

if(isset($_SESSION['zalogowany']) && $_SESSION['zalogowany']==true)//sprawdza czy uzytkownik jest zalogowany
{
	header('Location: ../content.php?page=character'); //jesli tak przekierowuje od razu do głównej zawartości strony
	exit(); // konczy wykonywanie skryptów na tej stronie 
	
}

?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="stylesheet" type="text/css" href="../css/loginpage.css">
	<title></title>
	<link href='https://fonts.googleapis.com/css?family=Audiowide&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Titillium+Web&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="contener">
	<div class="content">
	<h1>Panel logowania</h1>
	<form action="login.php" method="POST">
	Login: <br /><input type="text" name="login" /> <br />
	Hasło: <br /><input type="password" name="haslo" /> <br /><br />
	<input type="submit" value="Zaloguj" /><br /><br />
	Zapomniałeś hasła  ?   <a href="password_remember.php"><input type="button" value="Przypomnij hasło" /><a /><br /><br />
	Nie masz konta ? <a href="register.php"><input type="button" value="Zarejestruj się!" /><a />
	</form>
	<?php if(isset($_SESSION['error'])) echo "<br />".$_SESSION['error']; unset($_SESSION['error']);//wywala komunikat o nie poprawnym hasle lub loginie ?> 
	</div>
</div>

</body>
</html>