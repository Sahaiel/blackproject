<?php
	session_start();//pozwala korzystac z sesji // kazdy dokument ktory chce korzystac z sesji, musi posiadac ten wpis
	if((!isset($_POST['login'])) || (!isset($_POST['haslo']))) 
	{
		header('Location: login_page.php');
		exit();
	}
	require_once "db.php";
	mysqli_report(MYSQLI_REPORT_STRICT);
	try
	{
	$connect_db = new mysqli($db_host,$db_login,$db_password,$db_name);
	
		/*
		//skrócenie komunikatu o błędzie , przed new dajemy @
		*/
		if($connect_db->connect_errno!=0)
		{
			throw new Exception(mysqli_connect_errno());
		}
		else
		{
			
			$login = $_POST['login'];
			$haslo = $_POST['haslo'];
			$login = htmlentities($login, ENT_QUOTES, "UTF-8"); //uniemozliwia ataki sql injection , zmieniając znaki na encje np < na &lt;
			
			
			
			if($rezultat = @$connect_db->query(sprintf("SELECT * FROM users WHERE login='%s'",
			mysqli_real_escape_string($connect_db,$login)))) //zabezpiecza przed niebezpiecznymi znakami
			{
				if(!$rezultat) throw new Exception($connect_db->error);
				$ile_userow = $rezultat->num_rows; //sprawdza ile rekordow zwróciło zapytanie do bazy
				if($ile_userow==1)
				{
					$user = $rezultat->fetch_assoc(); //tworzy tablice która za numer komórki przyjmuje nazwe kolumny 
					
					if(password_verify($haslo,$user['password']))
					{
						$_SESSION['zalogowany'] = true;
						
						$_SESSION['login_S'] = $user['login']; //przypisanie zwroconej wartosci z kolumny login do sesji // sesja to taka zmienna globalna ktora mozna uzywac pomiedzy dokumentami
						$_SESSION['id_S'] = $user['id'];
						$_SESSION['haslo_S'] = $user['password'];
						$_SESSION['email_S'] = $user['email'];
						$rezultat->free_result(); //czyszczenie rezultatów zapytania
						unset($_SESSION['error']);
						header('Location: ../content.php?page=character');
					}
					else 
					{
						$_SESSION['error'] = "<span style='color: red;'>Nie poprawny login lub hasło</span>";
						header('Location: login_page.php');
					}
				}
				else 
				{
					$_SESSION['error'] = "<span style='color: red;'>Nie poprawny login lub hasło</span>";
					header('Location: login_page.php');
				}
			}
			
			
			$connect_db->close();
			
		}

	}
	catch(Exception $e) //wyjatek
	{
		echo '<span class="text_error">Błąd serwera. Prosimy o rejestrację w innym terminie</span>';//echo '<br />Informacja developerska: '.$e; //Informacja dla dev
	
	}
?>