<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="stylesheet" type="text/css" href="../css/loginpage.css">
	<link href='https://fonts.googleapis.com/css?family=Audiowide&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Titillium+Web&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<?php session_start(); ?>
</head>
<body>
<div class="contener">
	<div class="content">
	<h1>Zapomniałeś hasła ?</h1>
	<form method="POST">
	Podaj email: <br /><input type="text" name="email" /> <br /><br />
	<input type="submit" value="Zmień hasło" /><br />
	
	</form>
	<?php if(isset($_SESSION['remember_mail_error'])) echo $_SESSION['remember_mail_error']; unset($_SESSION['remember_mail_error']);?>
	</div>
</div>


<?php 

	
	if(isset($_POST['email']))
	{
		try
		{
		require_once "db.php";
		$connect_db = new mysqli($db_host,$db_login,$db_password,$db_name);
			if($connect_db->connect_errno!=0) throw new Exception(mysqli_connect_errno());
			else
			{
				
				$email = $_POST['email'];
				$email_B = filter_var($email, FILTER_SANITIZE_EMAIL); //stosuje filtr który oczyszcza email z niedozwolonych znaków
				if(filter_var($email_B, FILTER_VALIDATE_EMAIL)==false |($email_B!=$email)) //waliduje poprawnosc zapisu emaila
				{
					$_SESSION['remember_mail_error'] = "<div class='text_error'>Błedny email!</div>";
					header('Location: password_remember.php');
					exit();
		
				}
				
				$rezultat = $connect_db->query("SELECT * FROM users WHERE email='$email'"); //sprawdza czy taki email jest w bazie
				if(!$rezultat) throw new Exception($connect_db->error);
				if($rezultat->num_rows==0) 
				{
					$_SESSION['remember_mail_error'] = "<span class='text_error'>Ten email nie jest przypisany do żadnego konta!</span>"; 
					header('Location: password_remember.php');
					exit();
				}
				else
				{
					
					
					
					$token = md5(uniqid(rand(), true)); //token który bedzie wysyłany mailem z linkiem
					$expire_date = time() + 3600;
					
					$rezultat = $connect_db->query("INSERT INTO password_token VALUES ('$token', '$email', '$expire_date' )"); //wkłada token do bazy wraz z data wygaśnięcia
					if(!$rezultat) throw new Exception($connect_db->error);
					else
					{
						
						$link = substr($_SERVER['HTTP_REFERER'], 0, strrpos($_SERVER['HTTP_REFERER'],"/")+ 1 )."password_remember_verify.php?token=";
						$token = $link.$token; //tu musi sie znalesc link
						$tresc = "Oto link który pozwoli zresetować twoje hasło logowania 

".$token;
						$header = 	"From: blackproject.vxm.pl \nContent-Type:".
								' text/plain;charset="UTF-8"'.
								"\nContent-Transfer-Encoding: 8bit"; 
						if(mail($email, 'Przypomnienie hasła', $tresc, $header)) //trzeba będzie to przetestowac na serwerze
						{
							
							echo '<script>alert("Wiadomość została wysłana");</script>';
						}
						else 
						{
							$_SESSION['remember_mail_error'] = "<span class='text_error'>Błąd przy wysyłaniu maila!</span>"; 
							header('Location: password_remember.php');
							exit();
						}
						
						
					}
					
					
				}
				
				
				
				
			}
			$connect_db->close();
		}
		catch(Exception $e) //wyjatek
		{
			echo '<span class="text_error">Błąd serwera. Prosimy o rejestrację w innym terminie</span>';//echo '<br />Informacja developerska: '.$e; //Informacja dla dev
		
		}
	}



?>
</body>
</html>
