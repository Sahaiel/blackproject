<?php 
session_start(); 

if(isset($_SESSION['zalogowany']) && $_SESSION['zalogowany'] == true) //sprawdza czy uzytkownik jest zalogowany
{	
	header('Location: ../content.php?page=character');
	exit(); 
}

if(isset($_POST['email']))
{
	$walidacja = true; //Walidacja udana
	$login = $_POST['login'];
	if( (strlen($login) < 3) || (strlen($login) > 20) || (ctype_alnum($login)==false)) // sprawdza ilość znaków i czy znaki należą do alfanumerycznych
	{
		$walidacja = false;
		$_SESSION['e_login'] = "Login musi posiadać od 3 do 20 znaków i składać się tylko z cyfr i liter!";
		
	}
	//walidacja haseł
	$haslo = $_POST['haslo'];
	$p_haslo = $_POST['p_haslo'];
	if( (strlen($haslo) < 8) || (strlen($haslo) > 20))
	{
		$walidacja = false;
		$_SESSION['e_haslo'] = "Hasło musi mieć od 8 do 20 znaków!";
	}
	if($haslo!=$p_haslo)
	{
		$walidacja = false;
		$_SESSION['e_p_haslo'] = "Hasła muszą być takie same!";
	}
	$haslo_hash = password_hash($haslo, PASSWORD_DEFAULT); //kodowanie hasła 
	//
	//walidacja checkboxa
	if(!isset($_POST['regulamin']))
	{
		$walidacja = false;
		$_SESSION['e_regulamin'] = "Aby sie zarejestrować musisz zaakceptować regulamin!";
	}
	
	//
	$email = $_POST['email'];
	$email_B = filter_var($email, FILTER_SANITIZE_EMAIL); //stosuje filtr który oczyszcza email z niedozwolonych znaków
	if(filter_var($email_B, FILTER_VALIDATE_EMAIL)==false |($email_B!=$email)) //waliduje poprawnosc zapisu emaila
	{
		$walidacja = false;
		$_SESSION['e_email'] = "Błedny email!";
		
	}
	
	require_once 'db.php';
	mysqli_report(MYSQLI_REPORT_STRICT); //wyłącza wyświetlanie kodów o błędach
	try 
	{
		$connect_db = new mysqli($db_host,$db_login,$db_password,$db_name); //połączenie z bazą danych
			if($connect_db->connect_errno!=0)
			{
				throw new Exception(mysqli_connect_errno());
			}
			else
			{
				$rezultat = $connect_db->query("SELECT id FROM users WHERE email='$email'");
				if(!$rezultat) throw new Exception($connect_db->error); //rzuca nowy kod błedu wynikający ze złego przesłania kwerendy
				
				if($rezultat->num_rows>0)
				{
					$walidacja=false;
					$_SESSION['e_email'] = "Ten email jest już zajęty!";
				}
				
				$rezultat = $connect_db->query("SELECT id FROM users WHERE login='$login'");
				if(!$rezultat) throw new Exception($connect_db->error); 
				
				if($rezultat->num_rows>0)
				{
					$walidacja=false;
					$_SESSION['e_login'] = "Ten login jest już zajęty!";
				}
				
				if($walidacja==true)
				{
					//dodajemy uzytkownika do bazy
					if($connect_db->query("INSERT INTO users VALUES (NULL, '$login', '$haslo_hash', '$email')"))
					{
						if($connect_db->query("INSERT INTO resources VALUES ('$login', 100, 10, 0, 30, 1)")); //dodaje login,gold,diamond,experience,level
						else throw new Exception($connect_db->error);
						if(!$connect_db->query("INSERT INTO statistics VALUES ('$login', 5, 5, 5, 5, 10)")) throw new Exception($connect_db->error);
		
					
					}
					else
					{
						throw new Exception($connect_db->error);
					}
	
				}
				$connect_db->close();
				//do okodowania: przekierowanie do okna informacyjnego
				
			}
			
	}
	catch(Exception $e) //wyjatek
	{
		echo '<span class="text_error">Błąd serwera. Prosimy o rejestrację w innym terminie</span>';
		//echo '<br />Informacja developerska: '.$e; //Informacja dla dev
	}
		

}


?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title></title>
	<style> 
	.text_error {
		color: red;
	}
	</style>
	<link rel="stylesheet" type="text/css" href="../css/loginpage.css">
	<link href='https://fonts.googleapis.com/css?family=Audiowide&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Titillium+Web&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="contener">
	<div class="content">
	<h1>Panel rejestracji</h1>
	<form method="POST">
	Login: <br /><input type="text" name="login" /> <br />
	<?php if(isset($_SESSION['e_login'])) echo '<span class="text_error">'.$_SESSION['e_login'].'</span><br />'; unset($_SESSION['e_login']);?>
	Hasło: <br /><input type="password" name="haslo" /> <br />
	<?php if(isset($_SESSION['e_haslo'])) echo '<span class="text_error">'.$_SESSION['e_haslo'].'</span><br />'; unset($_SESSION['e_haslo']);?>
	Powtórz hasło: <br /><input type="password" name="p_haslo" /> <br />
	<?php if(isset($_SESSION['e_p_haslo'])) echo '<span class="text_error">'.$_SESSION['e_p_haslo'].'</span><br />'; unset($_SESSION['e_p_haslo']);?>
	Adres e-mail: <br /><input type="text" name="email" /> <br />
	<?php if(isset($_SESSION['e_email'])) echo '<span class="text_error">'.$_SESSION['e_email'].'</span><br />'; unset($_SESSION['e_email']);?>
	<label><input type="checkbox" name="regulamin" /> Akceptuję regulamin</label><br />
	<?php if(isset($_SESSION['e_regulamin'])) echo '<span class="text_error">'.$_SESSION['e_regulamin'].'</span><br />'; unset($_SESSION['e_regulamin']);?> <br />
	<input type="submit" value="Zarejestruj" /><br /><br />
	Masz już konto ? <a href="login_page.php"><input type="button" value="Zaloguj się!" /><a />

	</form>
	</div>
</div>


</body>